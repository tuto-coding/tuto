# **Documentation Coding**
***
## Table des matières
- [Class](https://gitlab.com/tuto-coding/tuto#class)
- [Namespaces](https://gitlab.com/tuto-coding/tuto#namespaces)
- [Typage](https://gitlab.com/tuto-coding/tuto#typage)
- [Propriété](https://gitlab.com/tuto-coding/tuto#propriete)

## Class

### Définition
>Une classe est l’implémentation d’un type de données (au même titre que int ou une structure). Un objet est une instance d’une classe.

### Classe main
La classe Main doit être déclarée et elle va servir de point d’entrée du programme
```cs
class Prog
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
    }
}
```
### Membres
Une classe peut contenir plusieurs éléments/membres : 

- Les champs : un champ d’une classe définit une donnée (type valeur) ou une référence vers une donnée (type référence) qui existe pour toute instance de la classe.

- Les méthodes : peut-être vu comme un traitement sur les données d’un objet.

- propriétés : peut être vu comme un couple de méthodes pour intercepter les accès (lecture/écriture) aux données d’un objet avec la syntaxe d’accès aux champs.

- Les indexeurs : propriété spéciale utilisant la syntaxe d’accès à un tableau []. 

- Les événements : facilite l’implémentation de la notion de programmation événementielle.

- Les types encapsulés dans la classe.

- Constructeur : Le constructeur permet d'instancier un objet. Il peut contenir ou non des paramètres.

Une classe doit contenir un constructeur, il est par défaut créé si il n’existe pas.
***

### UML
Ce langage permet de conceptualiser une idée afin de faciliter la mise en code derriere.

![Une classe en UML](/img/Exemple_classe_UML.PNG)
***
![Une classe voiture en UML](/img/Classe_Voiture_UML.jpg)

### Programme en C#
Ici le programme consiste à créer une class voiture et à instancier un objet voiture contenant la méthode rouler.
```cs
/* La classe Prog contient la classe main qui est le point d'entrée */
class Prog
{
    static void Main(string[] args)
    {
        /* Instanciation de la voiture du Directeur */ 
        Voiture Directeur = new Voiture("Peugeot", "208", 5, 100, 35000);
        /* Utilisation de la méthode rouler */
        Directeur.rouler();
    }
}
/* La classe voiture permettra d'instancier une voiture contenant des champs et une méthode rouler */
class Voiture
{
    /* Les champs */
    private string marque;
    private string type;
    private int nbPorte;
    private int puissance;
    private int kilometrage;
    /* Le constructeur de la voiture */
    public Voiture(string marqueVoiture, string typeVoiture, int nbPorteVoiture, int puissanceVoiture, int kilometrageVoiture){
        this.marque = marqueVoiture;
        this.type = typeVoiture;
        this.nbPorte = nbPorteVoiture;
        this.puissance = puissanceVoiture;
        this.kilometrage = kilometrageVoiture;
    }
    /* Méthode rouler */
    public void rouler(){
        try{
            Console.WriteLine("La voiture roule");
        }
        catch{
            Console.WriteLine("Démarrer la voiture avant de pouvoir rouler");
        }
    }
}
```


***
## Namespaces

### Définition

Les programmes C# sont organisés à l’aide d’espaces de noms. Ils vont permettre d'organiser le code à l'intérieur du namespace considéré mais également de faire référence à des namespaces externes.

Leur principal avantage est de permettre l'isolation des noms de classe utilisés au sein d'un espace de noms, comme nous pourrons le voir ci-dessous.

---

### Déclaration

Les espaces de noms sont implicitement **public** et la déclaration d’un espace de noms ne peut pas inclure de modificateurs d’accès.

Ils sont déclarés via le mot clé "namespace" suivi de l'identifiant de l'espace de nom, ici Ocean :
```
namespace Ocean
{
    class Poisson {}

    class Dauphin {}
}
```
Ils peuvent être imbriqués, par exemple :
```
namespace Ocean.Abysse
{
    class Poulpe {}

    class Cachalot {}
}
```
- équivaut à :
```
namespace Ocean
{
    namespace Abysse
    {
        class Poulpe {}

        class Cachalot {}
    }
}
```
- et équivaut également à :
```
namespace Ocean.Abysse
{
    class Poulpe {}
}

namespace Ocean.Abysse
{
    class Cachalot {}
}
```
> Il est intéressant de noter que nous déclarons ici Poulpe et Cachalot dans le même espace de nom Ocean.Abysse, dans la seconde déclaration nous n'aurions donc pas pu déclarer une autre classe Poulpe, qui aurait été en conflit avec la classe Poulpe de la première déclaration.

---

### Isolation

L'utilisation de namespace va permettre l'isolation des noms de classes. Ainsi le code ci-dessous présentera un conflit du fait de la double déclaration Ocean.Poulpe et Ocean.Poulpe :
```
namespace Ocean
{
    class Poulpe {}
}

namespace Ocean
{
    class Poulpe {}
}
```
A l'inverse, le code ci-dessous permettra l'utilisation d'un même nom de classe Ocean.Poulpe et Abysse.Poulpe :
```
namespace Ocean
{
    class Poulpe {}
}

namespace Abysse
{
    class Poulpe {}
}
```

---

### Using

Le mot clé "using" va permettre d'importer les membres de l'espace de nom cité. Par exemple en écrivant `using Ocean` je pourrais accéder à la classe Poulpe directement dans un autre espace de noms.

Par exemple :
```
using System;
using Ocean;

namespace Ocean {
   class Poulpe {
      public void quisuisje() {
        Console.WriteLine("Je suis un poulpe.");
      }
   }
}
class identifiezvous {
   static void Main(string[] args) {
      Poulpe p = new Poulpe();
      p.quisuisje();
   }
}
```
produira :
```
Je suis un poulpe.
```


>Si un espace de nom contient des noms de classe identiques à un autre espace de nom, il faudra alors appeler la classe via son espace de nom comme nous pourrons le voir ci-dessous.


---

### Exemple d'utilisation de noms de classe identiques

Nous allons voir ci-dessous un exemple d'isolation permis par l'utilisation d'espace de nom :
```
using System;

namespace Ocean {
   class Poulpe {
      public void quisuisje() {
        Console.WriteLine("Je suis un poulpe.");
      }
   }
}
namespace Abysse {
   class Poulpe {
      public void quisuisje() {
        Console.WriteLine("Je suis un poulpe abissal.");
      }
   }
}   
class identifiezvous {
   static void Main(string[] args) {
      Ocean.Poulpe p = new Ocean.Poulpe();
      Abysse.Poulpe pa = new Abysse.Poulpe();
      p.quisuisje();
      pa.quisuisje();
   }
}
```
produira :
```
Je suis un poulpe.
Je suis un poulpe abissal.
```

***
## Typage

### Définition 

Toutes les variables et les constantes sont typées, tout comme chaque expression qui fournit une valeur. Chaque déclaration de méthode spécifie un nom, le type, le type de valeur, le type de référence ou le type de sortie. La bibliothèque de classe .NET représente un large éventail de constructions. Il s’agit notamment du système de fichiers, des connexions réseau, des collections et des tableaux d’objets et de dates. Un programme C# classique utilise des types de la bibliothèque de classe et des types définis par l’utilisateur qui modélisent les concepts spécifiques au domaine de problème du programme.

Les informations stockées dans un type peuvent inclure les éléments suivants :

- Espace de stockage nécessaire pour une variable du type.
- Valeurs minimale et maximale que le type peut représenter.
- Membres (méthodes, champs, événements, etc.) que le type contient.
- Type de base dont le type est hérité.
- Interface(s) qu’elle implémente.
- Sortes d’opérations autorisées.

![alt text](img/type.webp)
___

### Exemple de code 

Le compilateur utilise des informations de type pour vous assurer que toutes les opérations effectuées dans votre code sont sécurisées de type. Par exemple, si vous déclarez une variable de type int, le compilateur vous permet d’utiliser la variable en multiplication. 

``` 

    static void Exemple()
        {
            Console.WriteLine("\ntest");
            int NB_1 = Convert.ToInt32(Console.ReadLine());
            int result;
            for (int i = 1; i <= 25; ++i)
            {
               result = i * NB_1;
                Console.WriteLine(result);
            }
        }
```

Le compilateur incorpore les informations de type dans le fichier exécutable sous forme de métadonnées. Le common language runtime (CLR) utilise ces métadonnées au moment de l’exécution pour garantir que le type est sécurisé lorsqu’il alloue et libère de la mémoire.

>Métadonnées : Une métadonnée est une donnée servant à définir ou décrire une autre donnée, quel qu'en soit le support.


Lorsque vous déclarez une variable ou une constante dans un programme, vous devez spécifier son type ou utiliser le var mot clé pour permettre au compilateur de déduire le type. Le "var" permet de l'essayer le compliteur trouver le type ( il se démerde tout seul) 
```
    float temperature;
    string name;
    MyClass myClass;

    char firstLetter = 'C';
    var limit = 3;
    int[] source = { 0, 1, 2, 3, 4, 5 };
    var query = from item in source
                where item <= limit
                select item;
    
```
Les types de paramètres de méthode et les valeurs de retour sont spécifiés dans la déclaration de méthode. La signature suivante montre une méthode qui nécessite un int argument d’entrée et retourne une chaîne :
```
class Technicien : Employe
        {
            public Technicien(string Nom, string Travail) : base(Nom) { }
            public override void GetDescription()
            {
                base.GetDescription();
                Console.Write("Fonction: Technicien/n");
            }
        }
```

Après avoir déclaré une variable, vous ne pouvez pas la déclarer avec un nouveau type et vous ne pouvez pas affecter une valeur non compatible avec son type déclaré. Par exemple, vous ne pouvez pas déclarer une int valeur booléenne, puis l’affecter à une truevaleur booléenne . Toutefois, les valeurs peuvent être converties en d’autres types, par exemple lorsqu’elles sont affectées à de nouvelles variables ou passées en tant qu’arguments de méthode. Une conversion de type qui n’entraîne pas de perte de données est effectuée automatiquement par le compilateur. Une conversion susceptible de causer la perte de données exige une variable cast dans le code source.

 En cas de problème, [voir](https://learn.microsoft.com/fr-fr/dotnet/csharp/fundamentals/types/).
***

# Propriété

Les membres de classes regroupent toutes les méthodes et attributs encapsulés dans une classe. Ces méthodes et attributs peuvent aussi bien être appelés depuis l’intérieur de la classe que depuis l’extérieur. L’accessibilité des membres de classe peut être configurée grâce aux modificateurs d’accès. Les accesseurs permettent de configurer l'accessibilité aux attributs d'une classe. En C#, un attribut associé à des accesseurs peut être déclaré sous la forme d'une propriété.


### Modificateurs d'accès

L'accessibilité d'un namespace peut être declarée grâce aux mot-clé **public**, **protected**, **private protected** et **private**.

- **public** : tout le code est accessible depuis n'importe quelle assembly.
- **protected** : le code est uniquement accessible depuis les classes dérivées, quelle que soit l'assembly.
- **protected private** protected : le code est uniquemment accessible depuis les classes dérivées de la même assembly.
- **private** : le code est uniquement accessible depuis la classe.


| Namespace | public |  protected | private protected | private
| ----------- | ----------- | ----------- | ----------- | ----------- | 
| Dans la classe | o | o | o | o
| Classe dérivée (même assembly) | o |  o | o | X
| Classe non dérivée (même assembly) | o |  X | X | X 
| Classe dérivée (assembly différent) | o |  o | X | X 
| Classe non dérivée (assembly différent) | o |  X | X | X 

Pour davanatage de précisions : [modificateurs d'accès en c#](https://learn.microsoft.com/fr-fr/dotnet/csharp/programming-guide/classes-and-structs/access-modifiers)


### Exemple d'utilisation : le mot clé protected


```
class Tuto
{

    static void Main(string[] args)
    {
        Person p1 = new Person();

        //Le niveau de protection "protected" ne permet pas d'acceder directement au code
        //person.sayHello();

        //Le code est accessible 
        Employee p2 = new Employee();
        p2.saySomething();
    }

}

class Person
{
    //Uniquement accessible aux classes dérivées
    protected void sayHello()
    {
        Console.WriteLine("Hello world");
    }
}

class Employee : Person
{
    public void saySomething()
    {
        sayHello();
    }
}

```

## Utilisation de propriétés

>Les accesseurs sont des méthodes qui permettent de contrôler l'accessibilité des attributs de classe. Les accesseurs permetent de restreindre les utilisations possible d'un attribut et de garantir le principe d'encapsulation.

```
class Tuto
{

    static void Main(string[] args)
    {
        Person p1 = new Person();
        Console.WriteLine("Nom {0}", p1.getName());
    }

}

class Person
{
    //Attribut déclarés privés
    private String name;

    //Déclaration getters et setters pour l'attribut name
    public String getName()
    {
        return this.name;
    }

    public String setName()
    {
        return this.name;
    }

}
```

## Utilisation d'une propriété

En C#, la déclaration d'une propriété permet de directement lier des accesseurs à un attribut.

```
class Tuto
{
    static void Main(string[] args)
    {
        Person p1 = new Person();

        //Utilisation propriété Name
        Console.WriteLine("Nom {0}", p1.Name);
    }
}

class Person
{

    private String name;

    //Déclaration propriété Name
    public String Name{
        get => name;
        set => name = value;
    }
    

}


```
Pour davanatage de précision : [propriétés en c#](https://learn.microsoft.com/fr-fr/dotnet/csharp/programming-guide/classes-and-structs/properties)



