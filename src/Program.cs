/* La classe Prog contient la classe main qui est le point d'entrée */
class Prog
{
    static void Main(string[] args)
    {
        /* Instanciation de la voiture du Directeur */ 
        Voiture Directeur = new Voiture("Peugeot", "208", 5, 100, 35000);
        /* Utilisation de la méthode rouler */
        Directeur.rouler();
    }
}
/* La classe voiture permettra d'instancier une voiture contenant des champs et une méthode rouler */
class Voiture
{
    /* Les champs */
    private string marque;
    private string type;
    private int nbPorte;
    private int puissance;
    private int kilometrage;
    /* Le constructeur de la voiture */
    public Voiture(string marqueVoiture, string typeVoiture, int nbPorteVoiture, int puissanceVoiture, int kilometrageVoiture){
        this.marque = marqueVoiture;
        this.type = typeVoiture;
        this.nbPorte = nbPorteVoiture;
        this.puissance = puissanceVoiture;
        this.kilometrage = kilometrageVoiture;
    }
    /* Méthode rouler */
    public void rouler(){
        Console.WriteLine("La voiture roule");
    }
}
