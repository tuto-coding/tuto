# **Typage**

## Définition 

Classification de différents éléments par types, notamment en biologie, en médecine (typage cellulaire) et en informatique (typage de données).

Toutes les variables et les constantes sont typées, tout comme chaque expression qui fournit une valeur. Chaque déclaration de méthode spécifie un nom, le type, le type de valeur, le type de référence ou le type de sortie. La bibliothèque de classe .NET représente un large éventail de constructions. Il s’agit notamment du système de fichiers, des connexions réseau, des collections et des tableaux d’objets et de dates. Un programme C# classique utilise des types de la bibliothèque de classe et des types définis par l’utilisateur qui modélisent les concepts spécifiques au domaine de problème du programme.

Les informations stockées dans un type peuvent inclure les éléments suivants :

- Espace de stockage nécessaire pour une variable du type.
- Valeurs minimale et maximale que le type peut représenter.
- Membres (méthodes, champs, événements, etc.) que le type contient.
- Type de base dont le type est hérité.
- Interface(s) qu’elle implémente.
- Sortes d’opérations autorisées.

![alt text](img/type.webp)
___

## Exemple de code 

Le compilateur utilise des informations de type pour vous assurer que toutes les opérations effectuées dans votre code sont sécurisées de type. Par exemple, si vous déclarez une variable de type int, le compilateur vous permet d’utiliser la variable en multiplication. 

``` 

    static void Exemple()
        {
            Console.WriteLine("\ntest");
            int NB_1 = Convert.ToInt32(Console.ReadLine());
            int result;
            for (int i = 1; i <= 25; ++i)
            {
               result = i * NB_1;
                Console.WriteLine(result);
            }
        }
```

Le compilateur incorpore les informations de type dans le fichier exécutable sous forme de métadonnées. Le common language runtime (CLR) utilise ces métadonnées au moment de l’exécution pour garantir que le type est sécurisé lorsqu’il alloue et libère de la mémoire.

>Métadonnées : Une métadonnée est une donnée servant à définir ou décrire une autre donnée, quel qu'en soit le support.


Lorsque vous déclarez une variable ou une constante dans un programme, vous devez spécifier son type ou utiliser le var mot clé pour permettre au compilateur de déduire le type. L’exemple suivant montre des déclarations de variable qui utilisent des types numériques intégrés et des types complexes définis par l’utilisateur :  
```
    float temperature;
    string name;
    MyClass myClass;

    char firstLetter = 'C';
    var limit = 3;
    int[] source = { 0, 1, 2, 3, 4, 5 };
    var query = from item in source
                where item <= limit
                select item;
    
```
Les types de paramètres de méthode et les valeurs de retour sont spécifiés dans la déclaration de méthode. La signature suivante montre une méthode qui nécessite un int argument d’entrée et retourne une chaîne :
```
    public string GetName(int ID)
{
    if (ID < names.Length)
        return names[ID];
    else
        return String.Empty;
}
private string[] names = { "Spencer", "Sally", "Doug" };
```

Après avoir déclaré une variable, vous ne pouvez pas la déclarer avec un nouveau type et vous ne pouvez pas affecter une valeur non compatible avec son type déclaré. Par exemple, vous ne pouvez pas déclarer une int valeur booléenne, puis l’affecter à une truevaleur booléenne . Toutefois, les valeurs peuvent être converties en d’autres types, par exemple lorsqu’elles sont affectées à de nouvelles variables ou passées en tant qu’arguments de méthode. Une conversion de type qui n’entraîne pas de perte de données est effectuée automatiquement par le compilateur. Une conversion susceptible de causer la perte de données exige une variable cast dans le code source.
